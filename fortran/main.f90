PROGRAM main
        !!$ use OMP_LIB
        implicit none
        !integer, parameter :: n=20
        !integer, dimension(n) :: a
        !integer            :: i, i_min, i_max, rang, nb_taches
        !!$OMP PARALLEL PRIVATE(rang,nb_taches,i_min,i_max)
        !        rang= OMP_GET_THREAD_NUM() ; nb_taches=OMP_GET_NUM_THREADS() ; i_min=n ; i_max=0
        !        ! print*,"Number of cores : ",nb_taches
        !        !$OMP DO SCHEDULE(STATIC,n/nb_taches)
        !                do i = 1, n
        !                        a(i) = int(i) ; i_min=min(i_min,i) ; i_max=max(i_max,i)
        !                end do
        !        !$OMP END DO NOWAIT
        !        ! print *,"Rang : ",rang,"; i_min :",i_min,"; i_max :",i_max
        ! !$OMP END PARALLEL
        ! print*,a

        integer, dimension(1) :: seed = (/3/)
        real, parameter :: pi = 3.1415926536
        real :: x1, x2
        real :: v1, v2

        CALL RANDOM_NUMBER(x1)
        CALL RANDOM_NUMBER(x2)

        v1 = sqrt(-2.*5.*log(x1))*cos(2*pi*x2)
        v2 = sqrt(-2.*5.*log(x1))*sin(2*pi*x2)

        print*, "x1 :",x1,"; x2 :",x2,"; v1 :", v1, "; v2 :",v2
        
END PROGRAM main
