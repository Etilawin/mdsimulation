program generator

    implicit none

    ! Physical parameters
    character(len=10) :: atom_name
    REAL :: mol_size, k, k0, r0, atom_weight, metric_tensor, epsilon, sigma, atom_charge
    INTEGER :: nb_atoms, nb_bonds

    namelist /parameters/ atom_name, mol_size, k, k0, r0, atom_weight, metric_tensor, epsilon, sigma, atom_charge

    ! Program parameters
    INTEGER :: ios
    INTEGER :: x,y,z
    INTEGER :: next_up, next_right, next_front, current

    ! Read parameters
    open(unit=1, file="config.nml", iostat=ios, action="read")
    if ( ios /= 0 ) stop "Error opening file config.nml"
    read(1, nml=parameters)
    close(1)

    print*,"Molecule size : ", mol_size
    print*,"Atom name : ", atom_name
    print*,"Atom weight : ", atom_weight
    print*,"Atom charge : ", atom_charge
    print*,"k (VDW) : ", k
    print*,"k0 (teth) : ", k0
    print*,"r0 (Bonds) : ", r0
    print*,"Metric tensor size : ", metric_tensor
    print*,"Epsilon (LJ potential for VDW) : ", epsilon
    print*,"Sigma (LJ potential for VDW) : ", sigma

    ! Process some calculus
    nb_atoms = INT(mol_size*mol_size*mol_size)
    nb_bonds = 3*mol_size*mol_size*mol_size - 3*mol_size*mol_size

    ! Write in FIELD file
    open(unit=2, file="FIELD", iostat=ios, action="write")
    if ( ios /= 0 ) stop "Error opening file FIELD"

    write(2,*) "Square molecule of", nb_atoms,"atoms",NEW_LINE('A')
    write(2,*) "UNITS K",NEW_LINE('A')
    write(2,*) "MOLECULES 1"
    write(2,*) "Unknown specie"
    write(2,*) "NUMMOLS 1"
    write(2,*) "ATOMS",nb_atoms
    write(2,*) atom_name,atom_weight,atom_charge,nb_atoms

    write(2,*) "TETH", nb_atoms

    DO z = 1, INT(mol_size), 1
            DO y = 1, INT(mol_size), 1
                    DO x = 1, INT(mol_size), 1
                        current = x + (y-1)*mol_size + (z-1)*mol_size*mol_size
                        write(2,*)"harm", current, k0
                    END DO
            END DO
    END DO

    write(2,*) "BONDS",nb_bonds

    DO z = 1, INT(mol_size), 1
            DO y = 1, INT(mol_size), 1
                    DO x = 1, INT(mol_size), 1
                        current = x + (y-1)*mol_size + (z-1)*mol_size*mol_size
                        if ( z < INT(mol_size) ) then
                            next_front = x + (y-1)*mol_size + z*mol_size*mol_size
                            write(2,*) "harm", current, next_front, k, r0
                        end if

                        if ( y < INT(mol_size) ) then
                            next_up = x + y*mol_size + (z-1)*mol_size*mol_size
                            write(2,*) "harm", current, next_up, k, r0
                        end if

                        if ( x < INT(mol_size) ) then
                            next_right = x+1 + (y-1)*mol_size + (z-1)*mol_size*mol_size
                            write(2,*) "harm", current, next_right, k, r0
                        end if
                    END DO
            END DO
    END DO

    write(2,*) "FINISH"
    write(2,*) "VDW 1"
    write(2,*) "  ",atom_name, atom_name, "lj", epsilon, sigma
    write(2,*) "CLOSE"

    close(2)

    open(unit=3, file="CONFIG", iostat=ios, action="write")
    if ( ios /= 0 ) stop "Error opening file CONFIG"

    write(3,*) "Square molecule of", nb_atoms,"atoms"
    write(3,*) 0,1,nb_atoms
    write(3,*) metric_tensor,0.,0.
    write(3,*) 0.,metric_tensor,0.
    write(3,*) 0.,0.,metric_tensor

    DO z = 1, INT(mol_size), 1
            DO y = 1, INT(mol_size), 1
                    DO x = 1, INT(mol_size), 1
                        current = x + (y-1)*mol_size + (z-1)*mol_size*mol_size
                        write(3,*) atom_name, current
                        write(3,*) x*r0, y*r0, z*r0
                    END DO
            END DO
    END DO

    close(unit=3, iostat=ios)
    if ( ios /= 0 ) stop "Error closing file unit 3"

end program generator
