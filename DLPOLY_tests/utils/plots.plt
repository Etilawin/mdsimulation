set grid
set xlabel "Temps (s)"
plot "Statis.dat" u 2:3 t 'total energy' w lp, "" u 2:4 t "temperature" w lp, "" u 2:(sqrt($7*$7 + $8*$8 + $9*$9)) t 'Stress' w lp
