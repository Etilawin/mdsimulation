program DumpSTATISDLP

implicit none

character*100 :: header, header2
integer :: reason,step, ndata, natomtypes,idata,iavdata,ntrans
real*8 :: time
real*8 :: toten, temp, confen, vdwen, elecen
real*8 :: bonden, en3body, en4body,tetheringen, entalpy
real*8 :: rottemp, totvir,vdwvir,elecvir,bondvir
real*8 :: vir3b, constrbondvir, tetheringvir, volume, coreshelltemp
real*8 :: coreshellen, coreshellvir, cellAng1, cellAng2, cellAng3
real*8 :: pmfvir, pressure, msd(4),Stress(9)

real*8 :: avtoten, avtemp, avconfen, avvdwen, avstress(9)

open(unit=1,file='STATIS',status='old',action='read')
open(unit=2,file='Statis.dat',status='unknown',action='write')
open(unit=3,file='AveStatis.dat',status='unknown',action='write')

write(2,*) "#(1)step, (2)time, (3)toten, (4)temp, (5)confen, (6)vdwen, (7)stressxx,(8)stressyy,(9)stresszz"
write(3,*) "#(1)step, (2)time, (3)toten, (4)temp, (5)confen, (6)vdwen, (7)stressxx,(8)stressyy,(9)stresszz"

!!!!!!!!!!!!transient in term of simulation step: !!!!!!!!!!
ntrans=0


read(1,*) header
! read(1,*) header2
reason=0
idata=0
iavdata=0
avtoten=0.d0
avtemp=0.d0
avconfen=0.d0
avvdwen=0.d0
avstress=0.d0
   do while(reason.eq.0)! IF (Reason < 0) THEN end of file reached

     read(1,*,IOSTAT=reason) step, time, ndata
     if(reason.ne.0) goto 11
     natomtypes = ndata - 36

     write(*,*) "read step= ", step

     read(1,*,IOSTAT=reason) toten, temp, confen, vdwen, elecen
     read(1,*,IOSTAT=reason) bonden, en3body, en4body,tetheringen, entalpy
     read(1,*,IOSTAT=reason) rottemp, totvir,vdwvir,elecvir,bondvir
     read(1,*,IOSTAT=reason) vir3b, constrbondvir, tetheringvir, volume, coreshelltemp
     read(1,*,IOSTAT=reason) coreshellen, coreshellvir, cellAng1, cellAng2, cellAng3
     if(natomtypes.eq.1) then
        read(1,*,IOSTAT=reason) pmfvir, pressure, msd(1), stress(1),stress(2)
        read(1,*,IOSTAT=reason) stress(3),stress(4),stress(5),stress(6),stress(7)
        read(1,*,IOSTAT=reason) stress(8),stress(9)
      elseif(natomtypes.eq.2) then
        read(1,*,IOSTAT=reason) pmfvir, pressure, msd(1), msd(2),stress(1)
        read(1,*,IOSTAT=reason) stress(2),stress(3),stress(4),stress(5),stress(6)
        read(1,*,IOSTAT=reason) stress(7),stress(8),stress(9)
      elseif(natomtypes.eq.3) then
        read(1,*,IOSTAT=reason) pmfvir, pressure, msd(1), msd(2), msd(3)
        read(1,*,IOSTAT=reason) stress(1),stress(2),stress(3),stress(4),stress(5)
        read(1,*,IOSTAT=reason) stress(6),stress(7),stress(8),stress(9)
      elseif(natomtypes.eq.4) then
        read(1,*,IOSTAT=reason) pmfvir, pressure, msd(1), msd(2), msd(3)
        read(1,*,IOSTAT=reason) msd(4), stress(1),stress(2),stress(3),stress(4)
        read(1,*,IOSTAT=reason) stress(5),stress(6),stress(7),stress(8),stress(9)
     elseif(natomtypes.gt.4) then
     write(*,*) "unsupported format (natomtype>4 or NPT or heatflux)"
     STOP
     endif

     write(2,*) step,time, toten, temp, confen, vdwen, stress(1),stress(5),stress(9)
     idata=idata+1
     if(step.gt.ntrans)then
        iavdata=iavdata+1
        avtoten= avtoten+toten
        avtemp = avtemp+temp
        avconfen=avconfen+confen
        avvdwen=avvdwen+vdwen
        avstress(1)=avstress(1)+stress(1)
        avstress(5)=avstress(5)+stress(5)
        avstress(9)=avstress(9)+stress(9)
        write(3,*) step,time,avtoten/iavdata, avtemp/iavdata,&
             avconfen/iavdata, avvdwen/iavdata, avstress(1)/iavdata,&
             avstress(5)/iavdata,avstress(9)/iavdata
     endif
  enddo
11 continue

    write(*,*) "Transient steps= ",ntrans, "( ",idata-iavdata, "values)"
    write(*,*) "Data averaged on ",iavdata, "  values"



end program DumpSTATISDLP
