subroutine print_bonds(x, y, z, mol_size, nb_cl, k, r0)
    IMPLICIT none
    INTEGER, INTENT(in) :: x,y,z
    INTEGER, INTENT(in) :: mol_size, nb_cl
    INTEGER :: na_pos, cl_pos
    CHARACTER(len=2) :: atom
    REAL :: k, r0

    ! Get atom name
    IF (modulo(x,2) == 1) THEN
        IF (modulo(y,2) == 1) THEN
            IF (modulo(z,2) == 1) THEN
                atom = "Cl"
            ELSE
                atom = "Na"
            END IF
        ELSE
            IF (modulo(z,2) == 1) THEN
                atom = "Na"
            ELSE
                atom = "Cl"
            END IF
        END IF
    ELSE
        IF (modulo(y,2) == 1) THEN
            IF (modulo(z,2) == 1) THEN
                atom = "Na"
            ELSE
                atom = "Cl"
            END IF
        ELSE
            IF (modulo(z,2) == 1) THEN
                atom = "Cl"
            ELSE
                atom = "Na"
            END IF
        END IF
    END IF

    IF (x+1 <= mol_size) THEN
        IF(atom == "Cl") THEN
            na_pos = nb_cl + ((x-1+1) + (y-1)*mol_size + (z-1)*mol_size*mol_size)/2 + 1
            cl_pos = ((x-1) + (y-1)*mol_size + (z-1)*mol_size*mol_size)/2 + 1
            write(1,*) "harm ", cl_pos, na_pos, k, r0
        ELSE
            na_pos = nb_cl + ((x-1) + (y-1)*mol_size + (z-1)*mol_size*mol_size)/2 + 1
            cl_pos = ((x-1+1) + (y-1)*mol_size + (z-1)*mol_size*mol_size)/2 + 1
            write(1,*) "harm ", na_pos, cl_pos, k, r0
        END IF
        !write(1,"A5") "harm "
    END IF
    IF (y+1 <= mol_size) THEN
        IF(atom == "Cl") THEN
            na_pos = nb_cl + ((x-1) + (y-1+1)*mol_size + (z-1)*mol_size*mol_size)/2 + 1
            cl_pos = ((x-1) + (y-1)*mol_size + (z-1)*mol_size*mol_size)/2 + 1
            write(1,*) "harm ", cl_pos, na_pos, k, r0
        ELSE
            na_pos = nb_cl + ((x-1) + (y-1)*mol_size + (z-1)*mol_size*mol_size)/2 + 1
            cl_pos = ((x-1) + (y-1+1)*mol_size + (z-1)*mol_size*mol_size)/2 + 1
            write(1,*) "harm ", na_pos, cl_pos, k, r0
        END IF
    END IF
    IF (z+1 <= mol_size) THEN
        IF(atom == "Cl") THEN
            na_pos = nb_cl + ((x-1) + (y-1)*mol_size + (z-1+1)*mol_size*mol_size)/2 + 1
            cl_pos = ((x-1) + (y-1)*mol_size + (z-1)*mol_size*mol_size)/2 + 1
            write(1,*) "harm ", cl_pos, na_pos, k, r0
        ELSE
            na_pos = nb_cl + ((x-1) + (y-1)*mol_size + (z-1)*mol_size*mol_size)/2 + 1
            cl_pos = ((x-1) + (y-1)*mol_size + (z-1+1)*mol_size*mol_size)/2 + 1
            write(1,*) "harm ", na_pos, cl_pos, k, r0
        END IF
    END IF
end subroutine print_bonds

subroutine print_bonds_from_nb(atom_number, mol_size, nb_cl, k, r0)
    IMPLICIT none
    INTEGER :: x,y,z
    INTEGER, INTENT(in) :: mol_size, nb_cl, atom_number
    INTEGER :: voisin_pos_x, voisin_pos_y, voisin_pos_z
    CHARACTER(len=2) :: atom
    REAL, intent(in) :: k, r0

    if ( atom_number > nb_cl ) then
        atom = "Na"
        x = 2*(atom_number - nb_cl)
    ELSE
        atom = "Cl"
        x = 2*atom_number - 1
    end if
    y = x / mol_size
    z = y / mol_size
    x = modulo(x, mol_size)
    y = modulo(y, mol_size)

    if ( atom_number > nb_cl ) then
        voisin_pos_x = ((x-1+1) + (y-1)*mol_size + (z-1)*mol_size*mol_size)/2 + 1
        voisin_pos_y = ((x-1) + (y-1+1)*mol_size + (z-1)*mol_size*mol_size)/2 + 1
        voisin_pos_z = ((x-1) + (y-1)*mol_size + (z-1+1)*mol_size*mol_size)/2 + 1
    else
        voisin_pos_x = nb_cl + ((x-1+1) + (y-1)*mol_size + (z-1)*mol_size*mol_size)/2 + 1
        voisin_pos_y = nb_cl + ((x-1) + (y-1+1)*mol_size + (z-1)*mol_size*mol_size)/2 + 1
        voisin_pos_z = nb_cl + ((x-1) + (y-1)*mol_size + (z-1+1)*mol_size*mol_size)/2 + 1
    end if

    if ( x+1 <= mol_size ) then
        write(1,*) "harm ", atom_number, voisin_pos_x, k, r0
    end if
    if ( y+1 <= mol_size ) then
        write(1,*) "harm ", atom_number, voisin_pos_y, k, r0
    end if
    if ( z+1 <= mol_size ) then
        write(1,*) "harm ", atom_number, voisin_pos_z, k, r0
    end if
end subroutine print_bonds_from_nb

subroutine print_position(x, y, z, distance, nb_cl, mol_size)
    INTEGER, intent(in) :: x, y, z, nb_cl, mol_size
    REAL, intent(in) :: distance
    CHARACTER(len=2) :: atom
    INTEGER :: atom_number
    REAL :: r_x, r_y, r_z

    ! rand(0) * (max-min) + min

    r_x = rand(0) * (distance / 2) - distance / 4
    r_y = rand(0) * (distance / 2) - distance / 4
    r_z = rand(0) * (distance / 2) - distance / 4

    !Get the atom name
    IF (modulo(x,2) == 1) THEN
        IF (modulo(y,2) == 1) THEN
            IF (modulo(z,2) == 1) THEN
                atom = "Cl"
            ELSE
                atom = "Na"
            END IF
        ELSE
            IF (modulo(z,2) == 1) THEN
                atom = "Na"
            ELSE
                atom = "Cl"
            END IF
        END IF
    ELSE
        IF (modulo(y,2) == 1) THEN
            IF (modulo(z,2) == 1) THEN
                atom = "Na"
            ELSE
                atom = "Cl"
            END IF
        ELSE
            IF (modulo(z,2) == 1) THEN
                atom = "Cl"
            ELSE
                atom = "Na"
            END IF
        END IF
    END IF

    if ( atom == "Cl" ) then
        atom_number = ((x-1) + (y-1)*mol_size + (z-1)*mol_size*mol_size)/2 + 1
    end if
    if ( atom == "Na" ) then
        atom_number = nb_cl + ((x-1) + (y-1)*mol_size + (z-1)*mol_size*mol_size)/2 + 1
    end if

    write(2,*) atom, atom_number
    write(2,*) (x+r_x)*distance, (y+r_y)*distance, (z+r_z)*distance
end subroutine print_position

PROGRAM generation
    IMPLICIT none

    REAL :: mol_size = 3.0
    INTEGER :: nummols = 1, nb_na, nb_cl, nb_bonds
    INTEGER :: x,y,z
    INTEGER :: atom_number
    REAL :: k,size,r0,distance

    print*, "Please enter the number of atoms on one line of the cube (ex : 3 would give a 3*3*3 cube) : "
    read* ,mol_size
    ! print*, "Please enter the number of molecules : "
    ! read* ,nummols
    print*, "Please enter k, for the harmonic potential between atoms : "
    read*, k
    print*, "Please enter r0 : "
    read*, r0

    nb_cl = CEILING(mol_size*mol_size*mol_size/2)
    nb_na = FLOOR(mol_size*mol_size*mol_size/2)
    nb_bonds = 3*mol_size*mol_size*mol_size - 3*mol_size*mol_size

    open(unit=1, file="FIELD")
        ! Title first
        write(1,*) "Square molecule of", INT(mol_size*mol_size*mol_size),"atoms",NEW_LINE('A')
        write(1,*) "UNITS K",NEW_LINE('A')
        write(1,*) "MOLECULES 1"
        write(1,*) "NaCl"
        write(1,*) "NUMMOLS",nummols
        write(1,*) "ATOMS",INT(mol_size*mol_size*mol_size)
        write(1,"(A4,F15.5,I10,I10)") "Na",22.98977,0,nb_na
        write(1,"(A4,F15.5,I10,I10)") "Cl",35.45300,0,nb_cl
        write(1,*) "BOUNDS",nb_bonds

        DO z = 1, INT(mol_size), 1
                DO y = 1, INT(mol_size), 1
                        DO x = 1, INT(mol_size), 1
                                ! Neighbors are always of the other type
                                call print_bonds(x,y,z,INT(mol_size), nb_cl, k, r0)
                        END DO
                END DO
        END DO
        ! DO atom_number = 1, INT(mol_size*mol_size*mol_size), 1
        !     call print_bonds_from_nb(atom_number, INT(mol_size), nb_cl, k, r0)
        ! END DO

        write(1,*) "FINISH"
        write(1,*) "VDW",4
        !write(1,*) "Na", "Cl, "buck", C1, C2, C3
        write(1,*) "CLOSE"
    close(1)

    open(unit=2, file="CONFIG")
        write(2,*) "NaCl molecule with ",INT(mol_size*mol_size*mol_size),"atoms"
        write(2,*) 0, 1, INT(mol_size*mol_size*mol_size*nummols)

        print*, "Please enter the size of the square tensor : "
        read*, size

        print*, "Please enter the distance between Na and Cl : "
        read*, distance

        write(2,*) size, 0.0, 0.0
        write(2,*) 0.0, size, 0.0
        write(2,*) 0.0, 0.0, size

        ! We suppose there is only one molecule
        DO z = 1, INT(mol_size), 1
                DO y = 1, INT(mol_size), 1
                        DO x = 1, INT(mol_size), 1
                                call print_position(x,y,z,distance,nb_cl,INT(mol_size))
                        END DO
                END DO
        END DO

    close(2)
END PROGRAM generation
